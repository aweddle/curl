<?php if (!defined('BASEPATH')) exit("No direct script access allowed");

$config = array(
    'update' => array(
        array(
            'field' => 'first_name',
            'label' => 'First Name',
            'rules' => 'required|alpha',
        ),
        array(
            'field' => 'last_name',
            'label' => 'Last Name',
            'rules' => 'required|alpha',
        ),
        array(
            'field' => 'display_name',
            'label' => 'Display Name',
            'rules' => 'required|callback_alpha_dash_space',
        ),
        array(
            'field' => 'avatar',
            'label' => 'Avatar',
            'rules' => 'required|trim',
        )
    ),
    'email' => array(
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'valid_email|trim',
        )
    )
);