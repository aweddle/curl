<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_controller
{
    public $data;

    public function __construct()
    {
        parent::__construct();

        //Load models/lang/helpers/libraries
        $this->load->helper('form');
        $this->lang->load('bitbucket');
        $this->load->library('bitbucket');
        $this->load->library('form_validation');
    }
    public function index()
    {
        $this->data->repo = $this->bitbucket->get_repos();
        $this->template->title($this->module_details['name'])
            ->build('admin/index', $this->data);
    }
    public function commit($url)
    {
        $this->data->commit = $this->bitbucket->get_commits($url);
        $this->template->title($this->module_details['name'])
            ->build('admin/commit', $this->data);
    }
    public function issues($url)
    {
        $this->data->issue = $this->bitbucket->get_issues($url);
        $this->template->title($this->module_details['name'])
            ->build('admin/issues', $this->data);
    }
    public function profile()
    {
        $this->data->email = $this->bitbucket->get_email();
        $this->data->profile = $this->bitbucket->get_profile();
        $this->template->title($this->module_details['name'])
            ->append_js('module::update.js')
            ->build('admin/profile', $this->data);
    }
    public function branches($url)
    {
        $this->data->branches = $this->bitbucket->get_branches($url);
        $this->template->title($this->module_details['name'])
        ->build('admin/branches', $this->data);
    }
    public function repo_view()
    {
        $this->data->repo = $this->bitbucket->get_all_repos();
        $this->template->title($this->module_details['name'])
            ->build('admin/repo_view', $this->data);
    }
    public function ajax_update()
    {
        if($this->form_validation->run('update') == true)
        {
            $this->bitbucket->update_user($_POST);
            echo json_encode(array('status'=> true, 'message'=>'Successfully updated profile!'));
        }
        else
        {
            echo json_encode(array('status'=>false, 'message'=>validation_errors()));
        }
    }
    public function ajax_email()
    {
        if($this->form_validation->run('email') == true)
        {
            $this->bitbucket->post_new_email($_POST, $email = $this->input->post('email'));
            echo json_encode(array('status'=>true, 'message'=>'Successfully added email'));
        }
        else
        {
            echo json_encode(array('status'=>false, 'message'=>validation_errors()));
        }
    }
    public function ajax_email_primary()
    {
        $this->bitbucket->update_email($_POST, $email = $this->input->post('email'));
    }
    public function alpha_dash_space($str)
    {
        $this->form_validation->set_message('alpha_dash_space', 'The Display Name field only accepts a-z, A-Z, spaces, and -');
        return ( ! preg_match("/^([-a-z_ ])+$/i", $str)) ? FALSE : TRUE;
    }
}
