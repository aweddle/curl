<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Bitbucket extends Module
{
    public $version = '1.0';

    public function info()
    {
        $info = array(
            'name' => array(
                'en' => 'Bitbucket'
            ),
            'description' => array(
                'en' => 'This module allows you to access information from your Bitbucket account'
            ),
            'frontend' => true,
            'backend' => true,
            'sections' => array(
                'Bitbucket' => array(
                    'name' => 'bitbucket:bitbucket',
                    'uri' => 'admin/bitbucket',
                ),
                'User Profile' => array(
                    'name' => 'bitbucket:userprofile',
                    'uri' => 'admin/bitbucket/profile/'
                ),
                'View All Repositories' => array(
                    'name' => 'bitbucket:allrepo',
                    'uri' => 'admin/bitbucket/repo_view/'
                )
            )
        );
        //Legacy support
        if ( CMS_VERSION < '2.2.0')
        {
            $info['frontend'] = true;
            $info['backend'] = true;
            $info['menu'] = 'Bitbucket';
        }
        return $info;
    }

    public function install()
    {
        $this->db->delete('settings', array('module' => 'bitbucket'));


        $bitbucket_setting = array(
            array(
                'slug' => 'bitbucket_user',
                'title' => 'Bitbucket User Name',
                'description' => 'Bitbucket username',
                '`default`' => '',
                '`value`' => '',
                '`options`' => '',
                'type' => 'text',
                'is_required' => 1,
                'is_gui' => 1,
                'module' => 'bitbucket'),
            array(
                'slug' => 'bitbucket_password',
                'title' => 'Bitbucket Password',
                'description' => 'Bitbucket Password',
                '`default`' => '',
                '`value`' => '',
                '`options`' => '',
                'type' => 'text',
                'is_required' => 1,
                'is_gui' => 1,
                'module' => 'bitbucket'),


        );


       // Run insert statement for all arrays in bitbucket_settings
        foreach($bitbucket_setting as $arr)
        {
           if ( !$this->db->insert('settings', $arr))
           {
               return FALSE;
           }
        }
           return TRUE;
    }
    public function uninstall()
    {
        $this->dbforge->drop_table('bitbucket');
        $this->db->delete('settings', array('module' => 'bitbucket'));
        return true;
    }
    public function upgrade($old_version)
    {
        //Upgrade logic goes here
    }
    public function help()
    {
        return "No help documentation has been added for this module";
    }
    public function admin_menu(&$menu)
    {
        if(isset($menu['Bitbucket']))
        {
            $menu['Bitbucket']['Bitbucket'] = 'admin/bitbucket';
        }
        else
        {
            $menu['Bitbucket'] = array(
                'Bitbucket' => 'admin/bitbucket'
            );
        };
    }
}
