
$(document).ready(function(){

    //Hide Update button
    $(".update").hide();

    //Store initial profile values on page load
    var fname = $('.fname').val();
    var lname = $(".lname").val();
    var disname = $('.disname').val();
    var uri = $(".uri").val();
    var avatar = $(".avatar").val();

    $('.edit').on('click', function(){

        //Hide edit button
        $(this).hide();

        //Show update button
        $('.update').show();

        //Enable text boxes for editing
        var tr = $(this).closest('tr');

        tr.find(":input").attr('disabled', false);
    });

    $('.update').on('click', function(){

        //Disable text boxes
        var tr = $(this).closest('tr');
        tr.find(":input").attr('disabled', true);

        //Hide update button
        $(this).hide();

        //Show edit button
        $(".edit").show();

        $.ajax({
            url: '/admin/bitbucket/ajax_update',
            type: 'POST',
            dataType: 'json',
            data:
            {
                first_name:   tr.find(":input[name=fname]").val(),
                last_name:    tr.find(":input[name=lname]").val(),
                display_name: tr.find(":input[name=disname]").val(),
                resource_uri: tr.find(":input[name=uri]").val(),
                avatar:       tr.find(":input[name=avatar]").val()
            },
            success: function(output)
            {
                if(output.status == true)
                {
                    //Displays string if edit is successful
                    var string='';
                    string+='<div class="alert success animated fadeIn">' + output.message + '</div>';
                    $('#error').html('').prepend(string);
                }
                else
                {
                    //Displays form validation errors
                    var string='';
                    string+='<div class="alert error animated fadeIn">' + output.message + '</div>';
                    $("#error").html('').prepend(string);

                    //Returns profile information to what it was on page load
                    tr.find(":input[name=fname]").attr('value', fname);
                    tr.find(":input[name=lname]").attr('value', lname);
                    tr.find(":input[name=disname]").attr('value', disname);
                    tr.find(":input[name=uri]").attr('value', uri);
                    tr.find(":input[name=avatar]").attr('value', avatar);
                }
            }
        });
    });

    $('#add').on('click', function(){

        //Hide add button
        $(this).hide();

        //Show emailadd div
        $('#emailadd').show();
    });

    $('#emailupdate').on('click', function(){

        //Hide emailadd div
        $('#emailadd').hide();

        //Show add button
        $("#add").show();

        $.ajax({
            url: '/admin/bitbucket/ajax_email',
            type: 'POST',
            dataType: 'json',
            data:
            {
                email:   $('.newemail').val()
            },
            success: function(output)
            {
                if(output.status == true)
                {
                    //Displays string is addition is successful
                    var string='';
                    string+='<div class="alert success animated fadeIn">' + output.message + '</div>';
                    $("#error").html('').prepend(string);
                }
                else
                {
                    //Displays form validation errors
                    var string='';
                    string+='<div class="alert error animated fadeIn">' + output.message + '</div>';
                    $("#error").html('').prepend(string);
                }
            }
        })
    });

    $('.primary').on('click', function(){

        var tr = $(this).closest('tr');

        $.ajax({
            url: '/admin/bitbucket/ajax_email_primary',
            type: "POST",
            dataType: 'json',
            data:
            {
                email:   tr.find(".emailval").text(),
                primary: true
            }
        })
    })

});