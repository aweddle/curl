
<?php defined('BASEPATH') or exit('No direct script access allowed');

class Bitbucket
{
    public $url = 'https://bitbucket.org/api/1.0/user';
    public $curl;

    public function __construct()
    {
        $this->ci=& get_instance();
    }
    public function get_repos()
    {
        $this->get_initialize_curl();
        return json_decode($this->get_curl_exec($this->curl));
    }
    public function get_commits($repo)
    {
        $this->get_initialize_curl('https://bitbucket.org/api/2.0/repositories/'.Settings::get('bitbucket_user').'/'.$repo.'/commits');
        return json_decode($this->get_curl_exec($this->curl));
    }
    public function get_issues($repo)
    {
        $this->get_initialize_curl('https://bitbucket.org/api/1.0/repositories/'.Settings::get('bitbucket_user').'/'.$repo);
        return json_decode($this->get_curl_exec($this->curl));
    }
    public function get_profile()
    {
        $this->get_initialize_curl('https://bitbucket.org/api/1.0/users/'.Settings::get('bitbucket_user'));
        return json_decode($this->get_curl_exec($this->curl));
    }
    public function get_email()
    {
        $this->get_initialize_curl('https://bitbucket.org/api/1.0/users/'.Settings::get('bitbucket_user').'/emails');
        return json_decode($this->get_curl_exec($this->curl));
    }
    public function get_branches($repo)
    {
        $this->get_initialize_curl('https://bitbucket.org/api/1.0/repositories/'.Settings::get('bitbucket_user').'/'.$repo.'/branches');
        return json_decode($this->get_curl_exec($this->curl));
    }
    public function get_all_repos()
    {
        $this->get_initialize_curl('https://bitbucket.org/api/1.0/user/repositories/');
        return json_decode($this->get_curl_exec($this->curl));
    }
    public function update_user($data)
    {
        $this->put_initialize_curl('https://bitbucket.org/api/1.0/user', $data);
        $this->put_post_curl_exec();
    }
    public function post_new_email($data, $email)
    {
        $this->post_initialize_curl('https://bitbucket.org/api/1.0/users/'.Settings::get('bitbucket_user').'/emails/'.$email, $data);
        $this->put_post_curl_exec();
    }
    public function update_email($data, $email)
    {
        $this->put_initialize_curl('https://bitbucket.org/api/1.0/users/'.Settings::get('bitbucket_user').'/emails/'.$email, $data);
        $this->put_post_curl_exec();
    }
    private function get_initialize_curl($u = '')
    {
        $url = ($u)? $u : $this->url;
        $this->curl = curl_init($url);
        $this->get_curl_options();
    }
    private function put_initialize_curl($u = '', $data)
    {
        $url = ($u)? $u : $this->url;
        $this->curl = curl_init($url);
        $this->put_curl_options($data);
    }
    private function post_initialize_curl($u = '', $data)
    {
        $url = ($u)? $u : $this->url;
        $this->curl = curl_init($url);
        $this->post_curl_options($data);
    }
    private function get_curl_options()
    {
        curl_setopt($this->curl, CURLOPT_USERPWD, Settings::get('bitbucket_user').':'.Settings::get('bitbucket_password'));
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-type: application/json'));
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, false);
    }
    private function put_curl_options($data)
    {
        curl_setopt($this->curl, CURLOPT_USERPWD, Settings::get('bitbucket_user').':'.Settings::get('bitbucket_password'));
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, false);
    }
    private function post_curl_options($data)
    {
        curl_setopt($this->curl, CURLOPT_USERPWD, Settings::get('bitbucket_user').':'.Settings::get('bitbucket_password'));
        curl_setopt($this->curl, CURLOPT_POST, true);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
    }
    private function get_curl_exec()
    {
        $result = curl_exec($this->curl);
        curl_close($this->curl);
        return $result;
    }
    private function put_post_curl_exec()
    {
        curl_exec($this->curl);
        curl_close($this->curl);
    }
}



