<div>
    <section class='title'>
        <h4><?php echo "All Branches for ".$this->uri->segment(4); ?></h4>
    </section>
    <section>
        <div>
            <table border="0">
                <thead>
                <tr>
                    <th><?php echo "Node"; ?></th>
                    <th><?php echo "Time"; ?></th>
                    <th><?php echo "Branch"; ?></th>
                    <th><?php echo "Message"; ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($branches as $info): ?>
                    <tr>
                        <td><?php echo $info->node ?></td>
                        <td><?php echo $info->branch ?></td>
                        <td><?php echo $info->timestamp ?></td>
                        <td><?php echo $info->message ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
</div>