<div>
    <section class='title'>
        <h4><?php echo "All Commits on ".$this->uri->segment(4); ?></h4>
    </section>
    <section>
        <div>
            <table border="0">
                <thead>
                <tr>
                    <th><?php echo "Hash"; ?></th>
                    <th><?php echo "Date"; ?></th>
                    <th><?php echo "Message"; ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($commit->values as $info): ?>
                    <tr>
                        <td><?php echo $info->hash ?></td>
                        <td><?php echo $info->date ?></td>
                        <td><?php echo $info->message ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
</div>