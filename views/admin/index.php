<?php if(Settings::get('bitbucket_user') && Settings::get('bitbucket_password') == null): ?>
<div>
    <section class="title">
        <h4><?php echo "Please enter a Bitbucket username and password in the settings panel" ?></h4>
    </section>
</div>
<?php else: ?>

<div>
    <section class='title'>
        <h4><?php echo "Repositories owned by ".Settings::get('bitbucket_user'); ?></h4>
</section>
<section>
    <div>
        <table border="0">
            <thead>
            <tr>
                <th><?php echo "Slug"; ?></th>
                <th><?php echo "Name"; ?></th>
                <th><?php echo "Language"; ?></th>
                <th><?php echo "Owner"; ?></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($repo->repositories as $info): ?>
                <tr>
                    <td class='slug'><?php echo $info->slug ?></td>
                    <td class='name'><?php echo $info->name ?></td>
                    <td class='language'><?php echo $info->language ?></td>
                    <td class='owner'><?php echo $info->owner ?></td>
                    <td><a href="/admin/bitbucket/commit/<?php echo $info->slug?>">View Commits</a></td>
                    <td><a href='/admin/bitbucket/issues/<?php echo $info->slug?>'>View Issues</a></td>
                    <td><a href='/admin/bitbucket/branches/<?php echo $info->slug?>'>View Branches</a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</section>
</div>
<?php endif; ?>