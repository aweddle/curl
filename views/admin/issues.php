<div>
    <section class='title'>
        <h4><?php echo "Issues for ".$this->uri->segment(4) ?></h4>
    </section>
    <section>
        <div>
            <table border="0">
                <thead>
                <tr>
                    <th><?php echo "Owner"; ?></th>
                    <th><?php echo "State"; ?></th>
                    <th><?php echo "Description"; ?></th>
                    <th><?php echo "Issues"; ?></th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $issue->owner ?></td>
                        <td><?php echo $issue->state ?></td>
                        <td><?php echo $issue->description ?></td>
                        <td><?php echo $issue->has_issues ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </section>
</div>