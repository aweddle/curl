<div id="error">
</div>
<div>
    <section class='title'>
        <h4><?php echo "User Profile for ".Settings::get('bitbucket_user'); ?></h4>
    </section>
    <section>
        <div>
            <table border="0">
                <thead>
                <tr>
                    <th><?php echo "Username"; ?></th>
                    <th><?php echo "First Name"; ?></th>
                    <th><?php echo "Last Name"; ?></th>
                    <th><?php echo "Display Name"; ?></th>
                    <th><?php echo "Resource URI"; ?></th>
                    <th><?php echo "Avatar: "; ?><img src ='<?php echo $profile->user->avatar ?>' height='32' width='32'/></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php echo form_open(); ?>
                    <tr>
                        <td><?php echo $profile->user->username ?></td>
                        <td><?php echo form_input(array('name'=>'fname', 'value'=>$profile->user->first_name, 'class'=>'fname', 'disabled'=>true)); ?></td>
                        <td><?php echo form_input(array('name'=>'lname', 'value'=>$profile->user->last_name, 'class'=>'lname', 'disabled'=>true)); ?></td>
                        <td><?php echo form_input(array('name'=>'disname', 'value'=>$profile->user->display_name, 'class'=>'disname', 'disabled'=>true)); ?></td>
                        <td><?php echo form_input(array('name'=>'uri', 'value'=>$profile->user->resource_uri, 'class'=>'uri', 'disabled'=>true)); ?></td>
                        <td><?php echo form_input(array('name'=>'avatar', 'value'=>$profile->user->avatar, 'class'=>'avatar', 'disabled'=>true)); ?></td>
                        <td><?php echo form_button(array('name'=>'edit', 'content'=>'Edit', 'class'=>'edit')); ?></td>
                        <td><?php echo form_button(array('name'=>'update', 'content'=>'Update', 'class'=>'update' )); ?></td>
                    </tr>
                </tbody>
                <?php echo form_close(); ?>
            </table>
        </div>
    </section>
</div>
<br/>
<br/>
<br/>
<br/>
<div>
    <section class='title'>
        <h4><?php echo "Emails associated with ".Settings::get('bitbucket_user'); ?></h4>
    </section>
    <section>
        <div>
            <table border="0">
                <thead>
                <tr>
                    <th><?php echo "Email"; ?></th>
                    <th title='Showcases if an email has been activated (1) or not (blank).'><?php echo "Active"; ?></th>
                    <th title='Showcases which email address is being primarily used (1) -- Only one per account'><?php echo "Primary"; ?></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($email as $mail): ?>
                <?php echo form_open(); ?>
                    <tr>
                        <td class ='emailval'><?php echo $mail->email ?></td>
                        <td><?php echo $mail->active ?></td>
                        <td><?php echo $mail->primary ?></td>
                        <td><?php echo form_button(array('name'=>'primary', 'content'=>'Make Primary Email Account', 'class'=>'primary')); ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                <?php echo form_close(); ?>
            </table>
        </div>
    </section>
</div>
<br />
<br />
<br />
<br />
<div>
    <section class = 'title'>
        <h4><?php echo "Associate a new email with ".Settings::get('bitbucket_user'); ?></h4>
    </section>
    <section>
        <div>
            <table border='0'>
                <thead>
                <tr>
                    <th><?php echo "New email"; ?></th>
                    <th><?php echo form_button(array('name'=>'add', 'content'=>'Add New Email', 'id'=>'add')); ?></th>
                </tr>
                </thead>
                <tbody id ='emailadd' hidden>
                <?php echo form_open(); ?>
                    <tr>
                        <td><?php echo form_input(array('name'=>'newemail', 'class'=>'newemail')); ?></td>
                        <td><?php echo form_button(array('name'=>'update', 'content'=>'Update', 'id'=>'emailupdate')); ?></td>
                    </tr>
                </tbody>
                <?php echo form_close(); ?>
            </table>
        </div>
    </section>
</div>
