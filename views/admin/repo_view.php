<div>
    <section class='title'>
        <h4><?php echo "All Repositories viewable for ".Settings::get('bitbucket_user'); ?></h4>
    </section>
    <section>
        <div>
            <table border="0">
                <thead>
                <tr>
                    <th><?php echo "Slug"; ?></th>
                    <th><?php echo "Name"; ?></th>
                    <th><?php echo "Language"; ?></th>
                    <th><?php echo "Owner"; ?></th>
                    <th><?php echo "Description"; ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($repo as $info): ?>
                    <tr>
                        <td><?php echo $info->slug ?></td>
                        <td><?php echo $info->name ?></td>
                        <td><?php echo $info->language ?></td>
                        <td><?php echo $info->owner ?></td>
                        <td><?php echo $info->description ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </section>
</div>